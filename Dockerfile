FROM golang:alpine

# Install git so we can fetch libraries
RUN apk update && apk upgrade && \
    apk add --no-cache git

# Go setup
ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 755 "$GOPATH"
WORKDIR /go/src/app

# Add go-wrapper install for dependencies here to avoid
# downloading or compiling again when code changes

# Copy app source
COPY src/app .

# Build app
RUN go-wrapper download
RUN go-wrapper install app

# Run app
CMD ["go-wrapper", "run"]
