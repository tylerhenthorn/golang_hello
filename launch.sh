#!/bin/bash

NAME="golang_hello"

if docker build -t $NAME .
  then docker run $NAME
  else echo "Container build failed."
    exit 1
fi
